const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


// Esquema de usuario
const usuarioSchema = new Schema({
    nombre: String,
    apellido: String,
    email: { type: String, unique: true, lowercase: true },
    password: String,
    rol: {type: Schema.Types.ObjectId, ref: "Rol" },
    edad: Number,
}, { timestamps: true });

// Métodos para encriptar clave    =============================================

usuarioSchema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Usuario', usuarioSchema);
