const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;

// Esquema
const menurolSchema = new Schema({
    rol: {type: Schema.Types.ObjectId, ref: "Rol" },
    opciones: [{type: Schema.Types.ObjectId, ref: "Menu" }]


});

module.exports = mongoose.model('Menurol', menurolSchema);
