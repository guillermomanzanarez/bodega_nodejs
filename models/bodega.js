const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


// Esquema de bodega
const bodegaSchema = new Schema({
    codigo: String,
    nombre: String,
    descripcion: String,
};


module.exports = mongoose.model('Bodega', bodegaSchema);
