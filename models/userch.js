const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


// Esquema de usuario
const usuariochSchema = new Schema({
    nombre: String,
    apellido: String,
    email: { type: String, unique: true, lowercase: true },
    password: String,
    rol: {type: Schema.ObjectId, ref: 'Rol' },
    edad: Number,

};

// Métodos================================================

userSchema.statics.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Usuario', usuarioSchema);
