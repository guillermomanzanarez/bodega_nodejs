const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


// Esquema de usuario
const rolSchema = new Schema({
    nombre: String,
    descripcion: String

});

module.exports = mongoose.model('Rol', rolSchema);
