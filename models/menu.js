const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');
const Schema = mongoose.Schema;


// Esquema
const menuSchema = new Schema({
    name: String,
    path: String

});

module.exports = mongoose.model('Menu', menuSchema);
