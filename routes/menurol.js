const express = require('express');
const router = require('express').Router();
const passport = require('passport');
// Models
const Menurol = require('../models/menurol');
const Rol = require('../models/rol');

// Static Pages ================================================================
router.get('/menurol', async(req, res) => {
	const roles = await Rol.find();
	res.render('menurol', {
			roles
		});
});

router.get('/menurol/:id', async(req, res) => {
  Menurol.findOne({ rol: req.params.id }).populate({ path: 'opciones' }).sort({"number":-1}).exec(function(err,rol){
    res.json(rol);
  });
});


router.post('/menurol', passport.authenticate('local-menus', {

  failureRedirect : '/menurol',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/')
});

module.exports = router;
