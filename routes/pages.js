const router = require('express').Router()
const passport = require('passport');
// Models
const User = require('../models/user')
const Rol = require('../models/rol')
const Menu = require('../models/menu')
const Menurol = require('../models/menurol')

// Static Pages ================================================================
router.get('/', async (req, res, next) => {
   const roles = await Rol.find();
    res.render('index',{
      roles});
})
router.get('/login', async (req, res, next) => {
   res.render('login');
})

router.post('/login', passport.authenticate('local-login', {
	failureRedirect : '/auth/login',
	failureFlash : false // flash mensajes
}), function(req, res, next)  {
	res.redirect('/')
});

router.get('/signup', async(req, res) => {
	const roles = await Rol.find();
  const usuarios = await User.find().populate({path: 'rol', select: 'nombre'});
	res.render('signup', {
			roles, usuarios
		});
});

router.post('/signup', async (req, res, next) => {
  const user = new User(
    {email: req.body.email,
    password: User.generateHash(req.body.password), //Se encripta el password
    alias: req.body.alias,
    edad: req.body.edad,
    rol: req.body.rol,
    nombre: req.body.nombre,
    apellido: req.body.apellido
  });
  await user.save();
  res.redirect('/signup');
});

router.get('/signup-2', async(req, res) => {
	const roles = await Rol.find();
  const usuarios = await User.find().populate({path: 'rol', select: 'nombre'});
	res.render('signup-2', {
			roles, usuarios
		});
});

router.post('/signup-2', async (req, res, next) => {
  const user = new User(
    {email: req.body.email,
    password: User.generateHash(req.body.password), //Se encripta el password
    alias: req.body.alias,
    edad: req.body.edad,
    rol: req.body.rol,
    nombre: req.body.nombre,
    apellido: req.body.apellido
  });
  await user.save();
  res.redirect('/signup-2');
});

router.get('/rol', async (req, res) => {
    const roles = await Rol.find();
    res.render('rol',{
      roles
    })
})

router.get('/menu', async (req, res, next) => {
    const menus = await Menu.find();
    res.render('menu',{menus});
})

router.get('/menurol', async(req, res) => {
	const roles = await Rol.find();
  const menus = await Menu.find();
  const menuroles = await Menurol.find().populate({path:'rol', select:'nombre'}).populate({path:'opciones', select:'name'});
  console.log(menuroles);
	res.render('menurol', {
			roles, menus, menuroles
		});
});

router.get('/menurol/:id', async(req, res) => {
  Menurol.findOne({ rol: req.params.id }).populate({ path: 'opciones' }).sort({"number":-1}).exec(function(err,rol){
    res.json(rol);
  });
});

//===================== Modificar datos en el registro de menú =================
router.get('/editmenu/:id', async (req, res, next) => {
  const menus = await Menu.findById(req.params.id);
  console.log(menus)
  res.render('editmenu', { menus });
});

router.post('/editmenu/:id', async (req, res, next) => {
  const { id } = req.params;
  await Menu.update({_id: id}, req.body);
  res.redirect('/menu');
});

//=========================== Eliminar Menú ====================================
router.get('/deletemenu/:id', async (req, res, next) => {
  let { id } = req.params;
  await Menu.remove({_id: id});
  res.redirect('/menu');
});

//============================ Eliminar Rol ====================================
router.get('/deleterol/:id', async (req, res, next) => {
  let { id } = req.params;
  await Rol.remove({_id: id});
  res.redirect('/rol');
});

//============================= Editar Usuario =================================
router.get('/edituser/:id', async (req,res, next) => {
const usuarios = await User.findById(req.params.id);
const roles = await Rol.find();
res.render('edituser',{usuarios, roles});
});

router.post('/edituser/:id', async (req, res, next) => {
const { id } = req.params;
if (User.rol === 'Administrador'){
await User.update({_id:id},
                    {email: req.body.email,
                    password: User.generateHash(req.body.password), //Se encripta el password
                    alias:    req.body.alias,
                    edad:     req.body.edad,
                    rol:      req.body.rol,
                    nombre:   req.body.nombre,
                    apellido: req.body.apellido
                  });
res.redirect('/signup');
} else {
  res.send('No tienes privilegios para modificar este usuario');
}
});

//=================== Eliminar Usuario =========================================
router.get('/deleteuser/:id', async (req, res, next) => {
let { id } = req.params;
await User.remove({_id: id});
res.redirect('/signup');
});

//Modifcar los roles ya registrados
router.get('/editrol/:id', async (req,res, next) => {
const roles = await Rol.findById(req.params.id);
res.render('editrol',{roles});
});

router.post('/editrol/:id', async (req,res,next) => {
const { id } = req.params;
await Rol.update({_id: id}, req.body);
res.redirect('/rol');
});

//======================== Vista de usuarios ===================================
router.get('/viewusers', async (req, res, next) => {
  const roles = await Rol.find();
  const usuarios = await User.find().populate({path: 'rol', select: 'nombre'});
  console.log(usuarios);
	res.render('viewusers', { roles, usuarios });
});

router.post('/viewusers', async (req, res, next) => {
  res.redirect('/viewusers');
});

module.exports = router;
