const express = require('express');
const router = require('express').Router();
const passport = require('passport');
// Models
const Menu = require('../models/menu');

// Static Pages ================================================================
router.get('/menu', function (req, res) {
    res.render('menu');
})

router.post('/menu', passport.authenticate('local-menu', {

  failureRedirect : '/menu',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/')
});

module.exports = router;
