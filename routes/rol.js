const express = require('express');
const router = require('express').Router();
const passport = require('passport');
// Models
const Rol = require('../models/rol');

// Static Pages ================================================================
router.get('/rol', async (req, res) => {
  const roles = await Rol.find();
    res.render('rol',{
      roles
    });
})

router.post('/rol', passport.authenticate('local-roles', {

  failureRedirect : '/rol',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/')
});

module.exports = router;
