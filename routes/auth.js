const router = require('express').Router()
const passport = require('passport');
const Rol = require('../models/rol');
const Menu = require('../models/menu');
const Menurol = require('../models/menurol');
const User = require('../models/user');

// Registro ====================================================================
router.get('/signup', async(req, res) => {
	const roles = await Rol.find();
	const usuarios = await User.find();

	res.render('signup', {
			roles, usuarios
		});
});

/*router.post('/signup', passport.authenticate('local-signup', {

  failureRedirect : '/',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/signup')
});*/

router.post('/signup', async (req, res, next) => {
  const user = new User({email: req.body.email,
		password: User.generateHash(password), //Se encripta el password
		alias: 		req.body.alias,
		edad: 		req.body.edad,
		rol: 			req.body.rol,
		nombre: 	req.body.nombre,
		apellido: req.body.apellido
	});
  await user.save();
  res.redirect('/signup');
});

router.get('/signup-2', async(req, res) => {
	const roles = await Rol.find();
  const usuarios = await User.find().populate({path: 'rol', select: 'nombre'});
	res.render('signup-2', {
			roles, usuarios
		});
});

//Registro de roles
router.post('/rol', passport.authenticate('local-roles', {

  failureRedirect : '/',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/rol')
});
//=================== Registro de Menu ==========================================
router.post('/menu', passport.authenticate('local-menu', {

  failureRedirect : '/',
	failureFlash : true // flash mensajes
}), function(req, res, next)  {
	res.redirect('/menu')
});

router.get('/menurol', async(req, res) => {
	const roles = await Rol.find();
	const menus = await Menu.find();
	const menuroles = await Menurol.find().populate({path:'rol', select:'nombre'}).populate({path:'opciones', select:'name'});
	res.render('menurol', {
			roles
		});
});

router.get('/menurol/:id', async(req, res) => {
	Menurol.findOne({ rol: req.params.id }).populate({ path: 'opciones' }).sort({"number":-1}).exec(function(err,rol){
    res.json(rol);
  });
});

router.post('/menurol', async (req, res, next) => {
	const roles = await Rol.find();
	const menus = await Menu.find();
	const menuroles = await Menurol.find().populate({path:'rol', select:'nombre'}).populate({path:'opciones', select:'name'});
	//console.log(req.body);
	var bodydata= req.body;
	var rol = bodydata.rol;
  var menu = bodydata.menu_items;
	var main = new Menurol();
  main.rol = rol;
  main.opciones = menu
  main.save(function(err){
             if(!err){
               res.render('menurol',{roles,menus, menuroles});
             }else {
               console.log(err);
               res.json({"err":"Error al momento de guardar","res":null});
             }
           });
	console.log(rol);
	res.render('menurol',{
			roles, menus, menuroles
		});
	res.json(req.body)
});

// Login    ====================================================================

router.get('/login', function(req, res, next)  {
	if (req.user) {
		res.redirect('/')
	} else {
		res.render('login')
	}
})

router.post('/login', passport.authenticate('local-login', {
	failureRedirect : '/auth/login',
	failureFlash : false // flash mensajes
}), function(req, res, next)  {
	res.redirect('/')
});

// CERRAR SESIÓN ===============================================================
router.get('/logout', function(req, res, next) {
	req.logout();
	res.redirect('/');
});

// =================== Para modificar los datos en el registro de menu =========
router.get('/editmenu/:id', async (req, res, next) => {
	const menus = await Menu.findById(req.params.id);
	console.log(menus)
	res.render('editmenu', { menus });
});

router.post('/editmenu/:id', async (req, res, next) => {
  const { id } = req.params;
  await Menu.update({_id: id}, req.body);
  res.redirect('/menu');
});

//=========================== Eliminar Menú ====================================
router.get('/deletemenu/:id', async (req, res, next) => {
  let { id } = req.params;
  await Menu.remove({_id: id});
  res.redirect('/menu');
});


//============Editar los roles registrados======================================
router.get('/editrol/:id', async(req, res, next) => {
const roles = await Rol.findById(req.params.id);
console.log(roles);
res.render('editrol', {roles});
})

router.post('/editrol/:id', async (req, res, next) => {
const { id } = req.params;
await Rol.update({_id: id}, req.body);
res.redirect('/rol');
});

router.get('/viewusers', async (req, res, next) => {
  const roles = await Rol.find();
  const usuarios = await User.find();
	res.render('viewusers', { roles, usuarios });
});

module.exports = router;
