const LocalStrategy = require('passport-local').Strategy;
// Models
const User = require('../models/user');
const Rol = require('../models/rol');
const Menu = require('../models/menu');
const Menurol = require('../models/menurol');


module.exports = function(passport) {

  // PARA INGRESAR =============================================================

  passport.use('local-login', new LocalStrategy({
      //
      usernameField : 'email',    // 'email'
      passwordField : 'password', // 'password'
      passReqToCallback : true // Retorna la respuesta.
  }, function(req, email, password, done) {

      // we lookup a user with a matching 'email'
      User.findOne({email: email}).then(function(user) {

          // if no user found
          if (!user) {
              // Si el usuario no existe
              return done(null, false);
          }
          // chequeamos que el password sea el correcto
          if (!user.validPassword(password)) {
              //Si la clave es incorrecta
              return done(null, false);
          }

          // otherwise, pass user object with no errors
          return done(null, user)
      }).catch(function(err) {done(err, false)});
  }));

//=============Registro de Roles======================================
passport.use('local-roles', new LocalStrategy({
  usernameField : 'nombre',
  passwordField : 'descripcion',
  passReqToCallback : true //
}, function(req, nombre, descripcion, done) {

  // Si el usuario ya ha iniciado sesión
  if (req.rol) {
      //
      return done(null, req.rol);
  }

  //Se chequea que el email no pertenezca a otro usuario.
  Rol.findOne({nombre : nombre}).then(function(rol) {

      // Se comprueba si se encuentra el nombre en la bd
      if (rol) {
          // Si el nombre existe en la bd.
          return done(null, false, req.flash('signupMessage', 'El rol ya existe'));
          //return done(null, false);
      }

      //De lo contrario se guarda la información en la base de datos
      new Rol({

          nombre: nombre,
          descripcion: descripcion

      }).save(function(err, savedUser) {
          if (err) {
              return done(err, false)
          }
          // se realiza el registro con éxito
          return done(null, savedUser);
      })
  }).catch(function(err) {done(err, false)});
}));

//==========================================================================

// Para el registro de cada menu ===========================================
passport.use('local-menu', new LocalStrategy({
  usernameField : 'name',
  passwordField : 'path',
  passReqToCallback : true //
}, function(req, name, path, done) {

  if (req.menu) {
      //
      return done(null, req.menu);
  }

  //Se chequea que el nombre exista en la base de datos
  Menu.findOne({name : name}).then(function(menu) {

      // Se comprueba si se encuentra el nombre en la bd
      if (menu) {
          // Si el nombre existe en la bd.
          return done(null, false, req.flash('signupMessage', 'El menú ya existe'));
          //return done(null, false);
      }

      //De lo contrario se guarda la información en la base de datos
      new Menu({
          name: name,
          path: path
      }).save(function(err, savedMenu) {
          if (err) {
              return done(err, false)
          }
          // se realiza el registro con éxito
          return done(null, savedMenu);
      })
  }).catch(function(err) {done(err, false)});
}));

//==========================================================================

    // =========================================================================
    // REGISTRO DE USUARIOS ====================================================
    // =========================================================================
    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true
    }, function(req, email, password, done) {
        //Se chequea que el email no pertenezca a otro usuario.
        User.findOne({email : email}).then(function(user) {
            // Se comprueba si se encuentra el email en la base de datos
            if (user) {
                // Si el correo existe en la bd.
                //return done(null, false, req.flash('signupMessage', 'El correo electrónico ya existe'));
                return done(null, false);
            }
            //De lo contrario se guarda la información en la base de datos
            new User({
                email: email,
                password: User.generateHash(password), //Se encripta el password
                alias: req.body.alias,
                edad: req.body.edad,
                rol: req.body.rol,
                nombre: req.body.nombre,
                apellido: req.body.apellido
            }).save(function(err, savedUser) {
                if (err) {
                    return done(err, false)
                }
                // se realiza el registro con éxito
                return done(null, savedUser);
                console.log(req.body);
            })
        }).catch(function(err) {done(err, false)});
    }));

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    })
};
